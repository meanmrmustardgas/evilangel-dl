#### Introduction ####

For the past few years, I have been downloading videos from kink.com so that the SO and I may watch them in a more attractive and intuitive fashion. Originally, I was doing this manually, and then I started using a series of shell scripts to download them via curl. Earlier this year, I decided to properly write a better solution, and I have been updating it to add new features and fix bugs.

A couple months ago, a user here on Reddit asked if I would be interested in writing a similar utility for evilangel.com and offered to foot the bill for a subscription. I agreed, and this is the fruit of that labor.


#### Features ####

* Allows archiving of individual shoots or full movies/channels.
* Allows selection of video quality.
* Creates Emby/Kodi compatible NFO files for movies and the scenes therein.

#### Screenshot ####

![Running](resources/running.png "Running")

#### Requirements ####

* Python 3
* Selenium
* Chrome webdriver
* python-requests
* tqdm

#### Usage ####

![Usage](resources/usage.png "Usage")

#### Examples? ####

Want to download just the video for a single shoot [Castings - Anya Krey, from Rocco's Game of Whores]?

    evilangel-dl https://www.evilangel.com/en/video/Castings---Anya-Krey/182495

Want to download all the scenes in a movie [Rocco's Game of Whores]?

    evilangel-dl https://www.evilangel.com/en/movie/Roccos-Game-Of-Whores/81345

Want to download all the scenes from all movies on the site [in shell]?

    for i in $(seq 1 51); do evilangel-dl https://members.evilangel.com/en/dvds/\?page=${i}; done

#### Where do I get it? ####

There is a git repository located [here](https://gitlab.com/meanmrmustardgas/evilangel-dl).
.
#### How can I report bugs/request features? ####

You can either PM me here, or send an email to meanmrmustardgas at protonmail dot com.

#### FAQs ####

###### This is awesome. Can I buy you beer/hookers? ######

Sure. If you want to make donations, you can do so via the following crypto addresses:

Monero: 468kYQ3vUhsaCa8zAjYs2CRRjiqNqzzCZNF6Rda25Qcz2L8g8xZRMUHPWLUcC3wbgi4s7VyHGrSSMUcZxWQc6LiHCGTxXLA

Ethereum: 0xa685951101a9d51f1181810d52946097931032b5

Bitcoin: 3CcNQ6iA1gKgw65EvrdcPMe12Heg7JRzTr

Bitcoin Cash: qqnmxahz479syfjztkugr8cqf6w50gkfq5pm2ae46s

Litecoin: MGzd3GdY9G5mGL7cFvf4hfvXdbTZrr1mSN

###### How can I contribute? ######

If you want to contribute you can feel free to submit patches via email, or use the tools available at gitlab.

###### Can you write something for site XXX? ######

In a word, "Maybe?" I don't find porn offensive, so I don't care much about site content assuming all parties are consenting adults. I do *not* however have a ton of free time, or excess money/storage to spend on archiving content that isn't of personal interest to me.

If you'd like a script for your favorite site, the likelihood of getting it written increases if you are willing to pay for the access while developing, and have some patience.

#### TODO: ####

* Finish thumbnail handling.
