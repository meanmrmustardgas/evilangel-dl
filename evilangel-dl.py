#!/usr/bin/python3
##############################################################################
# evilangel-dl v0.2 - Downloads evilangel.com videos and metadata.
#
# Copyright (C) 2020 MeanMrMustardGas <meanmrmustardgas at protonmail dot com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

import argparse
import json
import os
import re
import requests
from http.cookiejar import Cookie
from http.cookiejar import CookieJar
from pathlib import Path
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from tqdm import tqdm
from xml.sax.saxutils import escape

# Parse some Arguments
parser = argparse.ArgumentParser(description='''Download videos and metadata from
                                                EvilAngel.com''')
parser.add_argument("url", help="Scene, Movie, or Gallery URL.")
parser.add_argument("-l", "--login", help="Log in to evilangel.com",
                    action="store_true", default=False)
parser.add_argument("-q", "--quality", help="Select video download quality.",
                    choices=["160p", "240p", "360p", "480p", "540p", "720p",
                             "1080p"], default="1080p")
args = parser.parse_args()

chrome_options = Options()
chrome_options.add_argument("--disable-blink-features=AutomationControlled")
chrome_options.add_argument("window-size=1920,1280")

#if args.login is False:
#    chrome_options.add_argument("--headless")

browser = Chrome(options=chrome_options)

login_url = "https://members.evilangel.com/en/login"
cookie_file = os.path.expanduser("~/cookies.json")


def get_filename(url):
    """
    Get filename from content description header of URL.
    """
    r = requests.head(url, cookies=cj, allow_redirects=True)
    try:
        cd = r.headers['content-disposition']
    except KeyError:
        return None
    if not cd:
        return None
    f = re.findall('filename=(.+)', cd)
    if len(f) == 0:
        return None
    fname = f[0]
    if (fname[0] == fname[-1]) and fname.startswith(("'", '"')):
        return fname[1:-1]
    else:
        return fname


def download_file(url, fname):
    """
    Download url and save as filename with progressbar
    """
    fpath = Path(fname)
    dname = os.path.dirname(fname)
    if not os.path.isdir(dname):
        try:
            os.makedirs(os.path.expanduser(dname))
        except OSError as error:
            print(error)
    if Path.exists(fpath):
        r = requests.head(url, cookies=cj, allow_redirects=True)
        length = r.headers['Content-Length']
        fsize = Path(fname).stat().st_size
        if not int(length) > fsize:
            print("File " + os.path.basename(fname) + " exists: Skipping.\n")
            return 1

    chunk_size = 1024
    dl = requests.get(url, cookies=cj, stream=True, allow_redirects=True)
    with open(fname, "wb") as fout:
        with tqdm(unit="B", unit_scale=True, unit_divisor=1024, miniters=1,
                  desc=os.path.basename(fname), total=int(dl.headers.get('content-length'))
                  ) as pbar:
            for chunk in dl.iter_content(chunk_size=chunk_size):
                fout.write(chunk)
                pbar.update(len(chunk))


def save_cookies(driver, path):
    with open(path, 'w') as filehandler:
        json.dump(driver.get_cookies(), filehandler)


def load_cookies(path):
    cj = CookieJar()
    with open(path, 'r') as cfile:
        cookies = json.load(cfile)
        for c in cookies:
            if "members" in args.url:
                browser.add_cookie(c)

            if "expiry" not in c:
                expires = None
            else:
                expires = c['expiry']
            cookie = Cookie(version=0,
                            name=c['name'],
                            value=c['value'],
                            port=None,
                            port_specified=False,
                            domain=c['domain'],
                            domain_specified=False,
                            domain_initial_dot=False,
                            path=c['path'],
                            path_specified=True,
                            secure=c['secure'],
                            expires=expires,
                            discard=False,
                            comment=None,
                            comment_url=None,
                            rest={"HTTPOnly": None},
                            rfc2109=None)
            cj.set_cookie(cookie)
    return cj


def ea_login():
    browser.get(login_url)
    input("Press ENTER to close browser after login.")
    save_cookies(browser, cookie_file)
    browser.quit()
    print("Saved session cookies to file " + cookie_file)


def ea_get_metadata(browser):
    actors = []
    genres = []

    print("Extracting metadata.")

    if "members" in browser.current_url:
        title_xpath = "//div[starts-with(@class, 'dvdDesc')]/h2"
        director_xpath = "//ul[starts-with(@class, 'directedBy')]/li/a"
        desc_xpath = "//p[starts-with(@class, 'descriptionText')]"
        star_xpath = "//div/a[starts-with(@href, '/en/pornstar/')]"
        genre_xpath = "//div/a[contains(@href, 'categories')]"
        date_xpath = "//li[starts-with(@class, 'updatedOn')]"
    elif "/en/video/" in browser.current_url:
        title_xpath = "//h1[starts-with(@class, 'sceneTitle')]"
        director_xpath = "//div[contains(@class, 'sceneColDirectors')]/a"
        desc_xpath = "//p[starts-with(@class, 'sceneDesc')]"
        date_xpath = "//li[starts-with(@class, 'updatedDate')]"
        star_xpath = "//div[contains(@class, 'sceneColActors')]/a"
        genre_xpath = "//div[contains(@class, 'sceneColCategories')]/a"
    else:
        title_xpath = "//h3[starts-with(@class, 'dvdTitle')]"
        desc_xpath = "//p[starts-with(@class, 'descriptionText')]"
        star_xpath = "//div/a[starts-with(@href, '/en/pornstar/')]/img"
        genre_xpath = "//p[starts-with(@class, 'dvdCol')]/a"
        director_xpath = "//ul[starts-with(@class, 'directedBy')]/li/a"
        date_xpath = "//li[starts-with(@class, 'updatedOn')]"

    title = browser.find_element_by_xpath(title_xpath).text
    try:
        d = browser.find_element_by_xpath(director_xpath)
        director = d.get_attribute('title')
    except NoSuchElementException:
        director = "Director Not Found."

    releasedate = browser.find_element_by_xpath(date_xpath).text
    try:
        desc = browser.find_element_by_xpath(desc_xpath).text
    except NoSuchElementException:
        desc = "No Description Available."

    for a in browser.find_elements_by_xpath(star_xpath):
        if a.get_attribute('alt') is None:
            actors.append(a.get_attribute('title'))
        else:
            actors.append(a.get_attribute('alt'))

    for g in browser.find_elements_by_xpath(genre_xpath):
        genres.append(g.get_attribute('title'))
    metadata = {
                "title": title,
                "releasedate": releasedate,
                "director": director,
                "description": desc,
                "actors": actors,
                "genres": genres
                }
    return metadata


def write_metadata_nfo(metadata, fname):
    """
    Write metadata to emby compatible NFO file.
    """
    print("Writing metadata to file: " + os.path.basename(fname))
    fname = os.path.expanduser(fname)
    i = 0
    nfo = open(fname, "w")
    nfo.write('<?xml version="1.0" encoding="utf-8" standalone="yes"?>' + "\n")
    nfo.write("<movie>" + "\n")
    nfo.write("  <plot>" + escape(metadata['description']) + "</plot>\n")
    nfo.write("  <title>" + escape(metadata['title']) + "</title>\n")
    nfo.write("  <director>" + escape(metadata['director']) + "</director>\n")
    nfo.write("  <releasedate>" + escape(metadata['releasedate']) +
              "</releasedate>\n")
    for tag in metadata['genres']:
        nfo.write("  <genre>" + escape(tag.strip()) + "</genre>\n")
    nfo.write("  <studio>Kink.com</studio>\n")
    if metadata['actors'] is not None:
        for actor in metadata['actors']:
            nfo.write("  <actor>\n")
            nfo.write("    <name>" + escape(actor.strip()) + "</name>\n")
            nfo.write("    <type>Actor</type>\n")
            i += 1
            nfo.write("  </actor>\n")
    nfo.write("</movie>\n")
    nfo.close()


def ea_get_images(url):
    if "dvd" in url:
        front = browser.find_element_by_class_name("frontCoverImg").get_attribute('href')
        back = browser.find_element_by_class_name("backCoverImg").get_attribute('href')
    else:
        front = browser.find_element_by_class_name("SceneImage").get_attribute('href')

    if front is not None:
        fname = get_filename(front)
        print("Downlading image: " + fname + ".\n")
        download_file(front, fname)
    if back is not None:
        bname = get_filename(back)
        print("Downloading image: " + bname + ".\n")
        download_file(back, bname)


def ea_get_scene_urls(browser):
    scene_list = []
    shoot_urls = []
    wait = WebDriverWait(browser, 10)
    wait.until(lambda browser: browser.execute_script("return document.readyState") == "complete")
    print("looking for videos\n")
    while True:
        slen = len(scene_list)
        try:
            scene_list = wait.until(ec.visibility_of_all_elements_located((By.XPATH, '//a[starts-with(@href, "/en/video/") or starts-with(@href, "/en/movie/") or starts-with(@href, "/en/movie/")]')))
        except TimeoutException:
            break
        if slen >= len(scene_list):
            break
    for a in scene_list:
        url = a.get_attribute('href')
        if url not in shoot_urls:
            shoot_urls.append(url)
    return shoot_urls


def ea_get_dl_link(shoot_id, quality):
    quality_index = ["1080p", "720p", "540p", "480p", "360p", "240p", "160p"]
    index = quality_index.index(quality)
    fname = None
    while fname is None:
        try:
            dl_url = "https://members.evilangel.com/movieaction/download/" + shoot_id + "/" + quality_index[index]
            fname = get_filename(dl_url)
        except IndexError:
            return None
        index += 1
    return {"fname": fname, "url": dl_url}

if __name__ == '__main__':
    if args.login is True:
        ea_login()
        input("Press ENTER to quit...")
        browser.quit()
    else:
        if "members" in args.url:
            browser.get(login_url)

        cj = load_cookies(cookie_file)
        basepath = "./"
        browser.get(args.url)
        if "specialsavings" in browser.current_url:
            browser.get(args.url)
        shoot_urls = ea_get_scene_urls(browser)
        for shoot in shoot_urls:
            # Let's see if we're trying to get a whole "movie".
            if "movie" in shoot or "dvd" in shoot:
                # If so, lets grab the links to all the scenes and iterate through them.
                browser.get(shoot)
                mdata = ea_get_metadata(browser)
                path = basepath + mdata['title'] + "/scenes/"
                scene_urls = ea_get_scene_urls(browser)
                for scene in scene_urls:
                    browser.get(scene)
                    idnum = scene.split("/")[-1]
                    dl = ea_get_dl_link(idnum, args.quality)
                    if dl is not None:
                        download_file(dl['url'], path + dl['fname'])
                        sdata = ea_get_metadata(browser)
                        nfo = path + dl['fname'].replace("mp4", "nfo")
                        write_metadata_nfo(sdata, nfo)
                    else:
                        print("Download unavailable for scene " + idnum + ".\n")
            else:
                browser.get(shoot)
                try:
                    mtitle = browser.find_element_by_xpath("//a[starts-with(@class, 'dvdLink')]").get_attribute('title')
                except NoSuchElementException:
                    mtitle = None
                if mtitle is not None:
                    path = basepath + mtitle + "/scenes/"
                else:
                    path = basepath
                idnum = shoot.split("/")[-1]
                dl = ea_get_dl_link(idnum, args.quality)
                if dl is not None:
                    download_file(dl['url'], path + dl['fname'])
                    sdata = ea_get_metadata(browser)
                    nfo = path + dl['fname'].replace("mp4", "nfo")
                    write_metadata_nfo(sdata, nfo)
                else:
                    print("Download unavailable for scene " + idnum + ".\n")
        input("Press ENTER to quit...")
        browser.quit()
